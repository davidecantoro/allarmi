package it.unisalento.pas.smartcitywastemanagement.restcontrollers;

import it.unisalento.pas.smartcitywastemanagement.dto.FillingLevelDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import it.unisalento.pas.smartcitywastemanagement.domain.MeasureWasteBin;
import it.unisalento.pas.smartcitywastemanagement.dto.MeasureWasteBinDTO;
import it.unisalento.pas.smartcitywastemanagement.repositories.MeasureWasteBinRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@CrossOrigin
@RestController
@RequestMapping("/api/measurements")
public class MeasureWasteBinRestController {

    @Autowired
    MeasureWasteBinRepository measureWasteBinRepository;

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @GetMapping("/")
    public List<MeasureWasteBinDTO> getAllMeasurements() {
        List<MeasureWasteBinDTO> dtoList = new ArrayList<>();
        List<MeasureWasteBin> measurements = measureWasteBinRepository.findAll();
        for (MeasureWasteBin measurement : measurements) {
            MeasureWasteBinDTO dto = new MeasureWasteBinDTO();
            // Map properties from domain to DTO
            dto.setId(measurement.getId());
            dto.setTimestamp(measurement.getTimestamp());
            dto.setIdBin(measurement.getIdBin());
            dto.setFillingLevel(measurement.getFillingLevel());
            dto.setCitizen(measurement.getCitizen());
            dto.setWaste_disposal(measurement.getWaste_disposal());

            dtoList.add(dto);
        }
        return dtoList;
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN')")
    @GetMapping("/{id}")
    public MeasureWasteBinDTO getMeasurementById(@PathVariable String id) {
        MeasureWasteBin measurement = measureWasteBinRepository.findById(id).orElse(null);
        if (measurement == null) {
            return null;
        }
        MeasureWasteBinDTO dto = new MeasureWasteBinDTO();

        dto.setTimestamp(measurement.getTimestamp());
        dto.setIdBin(measurement.getIdBin());
        dto.setFillingLevel(measurement.getFillingLevel());
        return dto;
    }

    @PreAuthorize("hasAnyRole( 'ADMIN','EMPLOYEE','CITIZEN')")
    @PostMapping("/")
    public ResponseEntity<MeasureWasteBinDTO> addMeasurement(@RequestBody MeasureWasteBinDTO measureDTO) {
        if (measureDTO == null || measureDTO.getFillingLevel() == null || measureDTO.getIdBin() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST); // 400 Bad Request
        }

        MeasureWasteBin newMeasurement = new MeasureWasteBin();
        newMeasurement.setIdBin(measureDTO.getIdBin());
        newMeasurement.setTimestamp(measureDTO.getTimestamp());
        newMeasurement.setFillingLevel(measureDTO.getFillingLevel());
        newMeasurement.setCitizen(measureDTO.getCitizen());
        newMeasurement.setWaste_disposal(measureDTO.getWaste_disposal());

        measureWasteBinRepository.save(newMeasurement);

        // Set ID for the response
        measureDTO.setId(newMeasurement.getId());

        return new ResponseEntity<>(measureDTO, HttpStatus.CREATED); // 201 Created
    }
    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN','EMPLOYEE','CITIZEN')")
    @GetMapping("/latest/{idBin}")
    public ResponseEntity<MeasureWasteBinDTO> getLatestMeasurementByBinId(@PathVariable String idBin) {
        Optional<MeasureWasteBin> measurement = measureWasteBinRepository.findFirstByIdBinOrderByTimestampDesc(idBin);

        MeasureWasteBinDTO dto = new MeasureWasteBinDTO();
        if (measurement.isPresent()) {
            dto.setId(measurement.get().getId());
            dto.setTimestamp(measurement.get().getTimestamp());
            dto.setIdBin(measurement.get().getIdBin());
            dto.setFillingLevel(measurement.get().getFillingLevel());
        } else {
            // Imposta i valori predefiniti se non viene trovato nessun dato
            dto.setIdBin(idBin);
            dto.setFillingLevel(0.0); // Imposta fillingLevel a 0
            dto.setTimestamp(new Date()); // Imposta il timestamp corrente
        }

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN','EMPLOYEE','CITIZEN')")
    @PostMapping("/latest/batch")
    public ResponseEntity<List<MeasureWasteBinDTO>> getLatestMeasurementsByBinIds(@RequestBody List<String> idBins) {
        List<MeasureWasteBinDTO> dtos = new ArrayList<>();

        for (String idBin : idBins) {
            Optional<MeasureWasteBin> measurement = measureWasteBinRepository.findFirstByIdBinOrderByTimestampDesc(idBin);
            MeasureWasteBinDTO dto = new MeasureWasteBinDTO();
            if (measurement.isPresent()) {
                dto.setId(measurement.get().getId());
                dto.setTimestamp(measurement.get().getTimestamp());
                dto.setIdBin(measurement.get().getIdBin());
                dto.setFillingLevel(measurement.get().getFillingLevel());
            } else {
                dto.setIdBin(idBin);
                dto.setFillingLevel(0.0); // Imposta fillingLevel a 0
                dto.setTimestamp(new Date()); // Imposta il timestamp corrente
            }
            dtos.add(dto);
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('AGENCY', 'ADMIN','EMPLOYEE')")
    @PostMapping("/batch")
    public ResponseEntity<?> addMultipleMeasurements(@RequestBody List<MeasureWasteBinDTO> measureDTOList) {
        if (measureDTOList == null || measureDTOList.isEmpty()) {
            return new ResponseEntity<>("Lista di misurazioni vuota.", HttpStatus.BAD_REQUEST);  // 400 Bad Request
        }

        for (MeasureWasteBinDTO measureDTO : measureDTOList) {
            if (measureDTO.getFillingLevel() == null || measureDTO.getIdBin() == null) {

                return new ResponseEntity<>("Misurazione non valida trovata nella lista.", HttpStatus.BAD_REQUEST);  // 400 Bad Request
            }

            MeasureWasteBin newMeasurement = new MeasureWasteBin();
            // Set properties
            newMeasurement.setIdBin(measureDTO.getIdBin());
            newMeasurement.setTimestamp(measureDTO.getTimestamp());
            newMeasurement.setFillingLevel(measureDTO.getFillingLevel());
            newMeasurement.setWaste_disposal(measureDTO.getWaste_disposal());
            newMeasurement.setCitizen(measureDTO.getCitizen());
            measureWasteBinRepository.save(newMeasurement);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);  // 201 Created
    }

    @PreAuthorize("hasAnyRole('COMUNE', 'ADMIN','SCHEDULING')")
    @GetMapping("/filterByMonthAndYear")
    public List<MeasureWasteBin> getMeasurementsByMonthAndYear(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return measureWasteBinRepository.findByTimestampBetween(startDate.atStartOfDay(), endDate.atTime(23, 59, 59));
    }

}
