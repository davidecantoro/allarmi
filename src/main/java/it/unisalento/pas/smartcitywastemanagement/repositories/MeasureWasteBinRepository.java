package it.unisalento.pas.smartcitywastemanagement.repositories;

import it.unisalento.pas.smartcitywastemanagement.domain.MeasureWasteBin;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MeasureWasteBinRepository extends MongoRepository<MeasureWasteBin, String> {
    Optional<MeasureWasteBin> findFirstByIdBinOrderByTimestampDesc(String idBin);
    List<MeasureWasteBin> findByTimestampBetween(LocalDateTime start, LocalDateTime end);


}
